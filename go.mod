module gitlab.com/carymatt/carved-nfs

go 1.14

require (
	cloud.google.com/go v0.65.0
	github.com/container-storage-interface/spec v1.3.0
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/prometheus/client_golang v1.7.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	golang.org/x/net v0.0.0-20200904194848-62affa334b73
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43 // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	google.golang.org/grpc v1.31.0
	k8s.io/api v0.19.2
	k8s.io/apimachinery v0.19.2
	k8s.io/client-go v0.19.0
	k8s.io/klog v1.0.0
	k8s.io/utils v0.0.0-20200912215256-4140de9c8800
)
