FROM golang:1.14.2 as builder
WORKDIR /go/src/gitlab.com/carymatt/carved-nfs
# Be parsimonious with what we add to make building faster while fiddling with deploy.
ADD go.* ./
ADD vendor vendor
ADD pkg pkg
ADD cmd cmd
RUN go build -o bin/fio ./cmd/fio

# Install lots of tools to make it easy to debug. Note that fio is necessary.
FROM debian
RUN apt-get update && apt-get install -y \
  nfs-common     \
  dnsutils       \
  curl           \
  netcat         \
  procps         \
  fio            \
  && apt-get clean -y
COPY ./data/fio/hammer-data.fio /fio-jobs/
COPY --from=builder /go/src/gitlab.com/carymatt/carved-nfs/bin/fio /fio

ENTRYPOINT ["/fio"]