package driver

import (
	"strings"

	"k8s.io/utils/mount"
)

// NFS manages NFS mounts, and has some additional tools that probably should life on their own.
type NFS interface {
	Mount(path string) error
	UnMount(path string) error
	IsMaybeMounted(path string) (bool, error)
	BindMount(source, target string, readOnly bool) error
}

type nfs struct {
	addr    string
	share   string
	mounter mount.Interface
}

func NewNFS(addr, share string) NFS {
	return &nfs{addr, share, mount.New("")}
}

func (n *nfs) Mount(path string) error {
	// mount [-t $fstype] [-o $options] [$source] $target
	return n.mounter.Mount(strings.Join([]string{n.addr, n.share}, ":"), path, "nfs", []string{"nolock"})
}

func (n *nfs) UnMount(path string) error {
	return n.mounter.Unmount(path)
}

func (n *nfs) IsMaybeMounted(path string) (bool, error) {
	notMnt, err := n.mounter.IsLikelyNotMountPoint(path)
	return !notMnt, err
}

func (n *nfs) BindMount(source, target string, readOnly bool) error {
	options := []string{"bind", "nolock"}
	if readOnly {
		options = append(options, "ro")
	}
	return n.mounter.Mount(source, target, "nfs", options)
}
