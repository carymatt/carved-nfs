package driver

import (
	"testing"

	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
)

func TestGetPVCCapacity(t *testing.T) {
	testCases := []struct {
		name          string
		capacity      string
		expectedBytes int64
	}{
		{
			name:          "parse Gi",
			capacity:      "10Gi",
			expectedBytes: 10737418240,
		},
		{
			name:          "parse bytes",
			capacity:      "1024",
			expectedBytes: 1024,
		},
	}

	for _, tc := range testCases {
		pvc := &apiv1.PersistentVolumeClaim{
			Spec: apiv1.PersistentVolumeClaimSpec{
				Resources: apiv1.ResourceRequirements{
					Requests: apiv1.ResourceList{
						"storage": resource.MustParse(tc.capacity),
					},
				},
			},
		}
		bytes, err := getPVCCapacity(pvc)
		if err != nil {
			t.Errorf("Unexpected error %v for %v", err, tc.name)
		} else if bytes != tc.expectedBytes {
			t.Errorf("Expected %v bytes but saw %v for %v", tc.expectedBytes, bytes, tc.name)
		}
	}
}
