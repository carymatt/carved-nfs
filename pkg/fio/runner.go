package fio

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"k8s.io/klog"
)

const (
	jobRoot = "/fio-jobs"
	pkgRoot = "gitlab.com/carymatt/carved-nfs/data/fio"
)

type WatchFunc func(*Datum)

type FioRunner struct {
	id        int
	jobfile   string
	watchers  []WatchFunc
	finishers []chan<- struct{}
}

func NewFioRunner(id int, job string) (*FioRunner, error) {
	finders := []func() string{
		func() string { return job },
		func() string { return filepath.Join(jobRoot, job) },
		func() string {
			gopath := os.Getenv("GOPATH")
			if gopath == "" {
				klog.Fatalf("Missing GOPATH")
			}
			return filepath.Join(gopath, "src", pkgRoot, job)
		},
	}
	for _, f := range finders {
		jobfile := f()
		if _, err := os.Stat(jobfile); err == nil {
			return &FioRunner{id: id, jobfile: jobfile}, nil
		}
	}
	return nil, fmt.Errorf("Can't find job %s", job)
}

// AddWatcher adds a WatchFunc that will be called asynchronously when new fio output is available.
// This should not be called after the runner is started.
func (f *FioRunner) AddWatcher(fn WatchFunc) {
	f.watchers = append(f.watchers, fn)
}

// AddFinisher adds a channel that will be closed when the fio command has finished. This should
// not be called after the runner is started.
func (f *FioRunner) AddFinisher(ch chan<- struct{}) {
	f.finishers = append(f.finishers, ch)
}

func (f *FioRunner) Start(ctx context.Context) (*context.CancelFunc, error) {
	cancelCtx, cancel := context.WithCancel(ctx)
	cmd := exec.CommandContext(cancelCtx, f.command(), f.args()...)
	out, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	if err = cmd.Start(); err != nil {
		return nil, err
	}
	if err = f.startWatcher(out); err != nil {
		cancel()
		return nil, err
	}
	go func() {
		cmd.Wait()
		for _, finisher := range f.finishers {
			close(finisher)
		}
	}()

	return &cancel, nil
}

func (f *FioRunner) command() string {
	return "fio"
}

func (f *FioRunner) args() []string {
	return []string{
		"--minimal",
		"--status-interval=5s",
		f.jobfile,
		fmt.Sprintf("--filename_format=fiodata-%d-$jobnum-$filenum", f.id),
	}
}
