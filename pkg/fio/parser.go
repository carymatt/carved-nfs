package fio

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"

	"k8s.io/klog"
)

const (
	version3 = "3"
)

// Datum describes the data received by watchers.
type Datum struct {
	HasDataError bool
	DataError    error

	Job            string
	Group          string
	Errors         string
	ReadKb         float64
	ReadBandwidth  float64
	ReadIOPS       float64
	ReadMs         float64
	ReadBwMin      float64
	ReadBwMax      float64
	ReadBwMean     float64
	ReadSLat       float64
	ReadCLat       float64
	ReadTLat       float64
	ReadSLatMin    float64
	ReadCLatMin    float64
	ReadTLatMin    float64
	WriteKb        float64
	WriteBandwidth float64
	WriteIOPS      float64
	WriteRuntimeMs float64
	WriteBwMin     float64
	WriteBwMax     float64
	WriteBwMean    float64
	WriteSLat      float64
	WriteCLat      float64
	WriteTLat      float64
	WriteSLatMin   float64
	WriteCLatMin   float64
	WriteTLatMin   float64
}

// updateLine parses a full line of fio output and updates metrics etc. It may be called asynchronously.
func (f *FioRunner) updateLine(line string) {
	fields := strings.Split(line, ";")
	if len(fields) < 86 {
		d := &Datum{
			HasDataError: true,
			DataError:    fmt.Errorf("Not enough fields (%d) in %s", len(fields), line),
		}
		klog.Warning(d.DataError.Error())
		f.notifyWatchers(d)
		return
	}
	switch fields[0] {
	case version3:
		errs := []string{}
		parseFloat := func(s string) float64 {
			f, err := strconv.ParseFloat(s, 32)
			if err != nil {
				errs = append(errs, err.Error())
				return 0
			}
			return f
		}
		d := &Datum{
			Job:            fields[2],
			Group:          fields[3],
			Errors:         fields[4],
			ReadKb:         parseFloat(fields[5]),
			ReadBandwidth:  parseFloat(fields[6]),
			ReadIOPS:       parseFloat(fields[7]),
			ReadMs:         parseFloat(fields[8]),
			ReadSLat:       parseFloat(fields[11]),
			ReadCLat:       parseFloat(fields[15]),
			ReadTLat:       parseFloat(fields[39]),
			ReadSLatMin:    parseFloat(fields[9]),
			ReadCLatMin:    parseFloat(fields[13]),
			ReadTLatMin:    parseFloat(fields[37]),
			ReadBwMin:      parseFloat(fields[41]),
			ReadBwMax:      parseFloat(fields[42]),
			ReadBwMean:     parseFloat(fields[44]),
			WriteKb:        parseFloat(fields[46]),
			WriteBandwidth: parseFloat(fields[47]),
			WriteIOPS:      parseFloat(fields[48]),
			WriteRuntimeMs: parseFloat(fields[49]),
			WriteBwMin:     parseFloat(fields[82]),
			WriteBwMax:     parseFloat(fields[83]),
			WriteBwMean:    parseFloat(fields[85]),
			WriteSLat:      parseFloat(fields[52]),
			WriteCLat:      parseFloat(fields[55]),
			WriteTLat:      parseFloat(fields[80]),
			WriteSLatMin:   parseFloat(fields[50]),
			WriteCLatMin:   parseFloat(fields[54]),
			WriteTLatMin:   parseFloat(fields[78]),
		}
		if len(errs) > 0 {
			d.HasDataError = true
			d.DataError = fmt.Errorf("parse error in %s: %s", line, strings.Join(errs, " ; "))
			klog.Warning(d.DataError.Error())
		}
		f.notifyWatchers(d)
	default:
		d := &Datum{
			HasDataError: true,
			DataError:    fmt.Errorf("Ignored line with bad fio version: %s", fields[0]),
		}
		klog.Warning(d.DataError.Error())
		f.notifyWatchers(d)
	}
}

// startWatcher starts a watcher on the reader from a process output pipe and buffers data before calling updateLine.
func (f *FioRunner) startWatcher(r io.Reader) error {
	scanner := bufio.NewScanner(r)
	go func() {
		for scanner.Scan() {
			f.updateLine(scanner.Text())
		}
		if err := scanner.Err(); err != nil {
			klog.Warningf("fio output error: %v", err)
		}
	}()
	return nil
}

func (f *FioRunner) notifyWatchers(d *Datum) {
	for _, w := range f.watchers {
		w(d)
	}
}
