.PHONY: help mountpod cluster

PROJECT = mattcary-snapshot
CLUSTER_NAME = carved-nfs

help:
	@echo Rules are driver, mountpod, cluster, etc

gofmt:
	for d in pkg cmd test; do find $$d -name \*.go | xargs gofmt -w -s; done

k8s-integration:
	go build -o bin/k8s-integration ./test/k8s-integration

driver:
	docker build -f Dockerfile.driver -t carved-nfs-driver .
	docker tag carved-nfs-driver gcr.io/$(PROJECT)/carved-nfs-driver:latest
	docker push gcr.io/$(PROJECT)/carved-nfs-driver:latest

docker-%:
	docker build -f Dockerfile.$* -t $* .
	docker tag $* gcr.io/$(PROJECT)/$*:latest
	docker push gcr.io/$(PROJECT)/$*:latest

mountpod:
	docker build -f Dockerfile.mountpod -t mountpod .
	docker tag mountpod gcr.io/$(PROJECT)/mountpod:latest
	docker push gcr.io/$(PROJECT)/mountpod:latest

fio:
	docker build -f Dockerfile.fio -t fio .
	docker tag fio gcr.io/$(PROJECT)/fio:latest
	docker push gcr.io/$(PROJECT)/fio:latest

cluster:
	gcloud beta container clusters create $(CLUSTER_NAME) \
		--release-channel rapid \
		--addons GcePersistentDiskCsiDriver \
		--no-enable-basic-auth 
