package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"

	"k8s.io/client-go/util/homedir"
	"k8s.io/klog"
)

var (
	parallel = flag.Bool("parallel", true, "run tests in parallel")
	focus    = flag.String("focus", "", "extra focus (in addition to External Storage)")
)

func init() {
	klog.InitFlags(nil)
}

func main() {
	flag.Parse()

	err := runE2E()
	if err != nil {
		klog.Fatalf("e2e test failed: %v", err)
	}
}

func gopath() (string, error) {
	goPath, ok := os.LookupEnv("GOPATH")
	if !ok {
		return filepath.Join(homedir.HomeDir(), "go"), nil
	}
	return goPath, nil
}

func packageDir() (string, error) {
	goPath, err := gopath()
	if err != nil {
		return "", fmt.Errorf("GOPATH env variable missing: %w", err)
	}
	return filepath.Join(goPath, "src/gitlab.com/carymatt/carved-nfs"), nil
}

func k8sDir() (string, error) {
	goPath, err := gopath()
	if err != nil {
		return "", fmt.Errorf("GOPATH env variable missing: %w", err)
	}
	k8sDir := filepath.Join(goPath, "src/k8s.io/kubernetes")
	if _, err := os.Stat(k8sDir); os.IsNotExist(err) {
		return "", fmt.Errorf("k8s.io/kubernetes source not found")
	}
	return k8sDir, nil
}

func getKubeConfig() (string, error) {
	config, ok := os.LookupEnv("KUBECONFIG")
	if ok {
		return config, nil
	}
	homeDir, ok := os.LookupEnv("HOME")
	if !ok {
		return "", fmt.Errorf("HOME env not set")
	}
	kubeconfig := filepath.Join(homeDir, ".kube/config")
	if _, err := os.Stat(kubeconfig); os.IsNotExist(err) {
		return "", fmt.Errorf("~/.kube/config does not exist")
	}
	return kubeconfig, nil
}

func runCommand(action string, cmd *exec.Cmd) error {
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr

	fmt.Printf("%s\n", action)
	fmt.Printf("%s\n", cmd.Args)

	err := cmd.Start()
	if err != nil {
		return err
	}

	err = cmd.Wait()
	if err != nil {
		return err
	}
	return nil
}

func buildKubernetes(k8sDir, command string) error {
	cmd := exec.Command("make", "-C", k8sDir, command)
	err := runCommand("k8s build", cmd)
	if err != nil {
		return fmt.Errorf("failed to build: %w", err)
	}
	return nil
}

func runE2E() error {
	pkgDir, err := packageDir()
	if err != nil {
		return err
	}
	k8sDir, err := k8sDir()
	if err != nil {
		return err
	}
	err = buildKubernetes(k8sDir, "WHAT=test/e2e/e2e.test")
	if err != nil {
		return fmt.Errorf("e2e.test build failed: %w", err)
	}
	err = buildKubernetes(k8sDir, "ginkgo")
	if err != nil {
		return fmt.Errorf("gingko build failed: %w", err)
	}
	err = buildKubernetes(k8sDir, "kubectl")
	if err != nil {
		return fmt.Errorf("kubectl build failed: %w", err)
	}

	tmplName := filepath.Join(pkgDir, "test/k8s-integration/config/test-config.yaml.input")
	configTmpl, err := template.ParseFiles(tmplName)
	if err != nil {
		return fmt.Errorf("Template parse error: %w", err)
	}
	configName := strings.TrimSuffix(tmplName, ".input")
	f, err := os.Create(configName)
	if err != nil {
		return fmt.Errorf("Can't create config template output %s: %w", configName, err)
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	err = configTmpl.Execute(w, struct{ PackageDir string }{
		PackageDir: pkgDir})
	if err != nil {
		return fmt.Errorf("Can't execute template: %w", err)
	}
	w.Flush()

	kubeconfig, err := getKubeConfig()
	if err != nil {
		return err
	}
	os.Setenv("KUBECONFIG", kubeconfig)

	testFocus := "External.Storage"
	if *focus != "" {
		testFocus += ".*" + *focus
	}
	testArgs := []string{
		"--ginkgo.focus=" + testFocus,
		"--ginkgo.skip=\\[Disruptive\\]|\\[Serial\\]",
		fmt.Sprintf("--storage.testdriver=%s", configName),
	}

	cluster, ok := os.LookupEnv("CLUSTER")
	if !ok {
		return fmt.Errorf("CLUSTER unset")
	}

	project, ok := os.LookupEnv("PROJECT")
	if !ok {
		return fmt.Errorf("PROJECT unset")
	}

	zone, ok := os.LookupEnv("ZONE")
	if !ok {
		return fmt.Errorf("ZONE unset")
	}

	kubetestArgs := []string{
		"--test",
		"--check-version-skew=false",
		"--up=false",
		"--down=false",
		"--provider=gke",
		"--gcp-network=default",
		"--check-version-skew=false",
		"--deployment=gke",
		"--cluster=" + cluster,
		"--gcp-project=" + project,
		"--gcp-zone=" + zone,
		"--gke-environment=prod",
		fmt.Sprintf("--test_args=%s", strings.Join(testArgs, " ")),
	}

	if *parallel {
		kubetestArgs = append(kubetestArgs, "--ginkgo-parallel")
	}

	err = os.Chdir(k8sDir)
	if err != nil {
		return err
	}
	err = runCommand("Running tests", exec.Command("kubetest", kubetestArgs...))
	if err != nil {
		return fmt.Errorf("kubetest failed: %w", err)
	}

	return nil
}
