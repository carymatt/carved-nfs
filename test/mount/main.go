package main

import (
	"os"
	"path/filepath"

	"gitlab.com/carymatt/carved-nfs/test/util"
	"k8s.io/klog"
)

func main() {
	client := util.NewClientConfig()
	if err := client.KubectlApply(filepath.Join(util.PackageRoot(), "deploy/nfs.yaml")); err != nil {
		klog.Fatalf("Could not apply nfs.yaml: %w", err)
	}
	if err := client.KubectlWaitCondition("pod", "nfs-server", "Ready"); err != nil {
		klog.Fatalf("Problem waiting for nfs-server: %w", err)
	}
}
