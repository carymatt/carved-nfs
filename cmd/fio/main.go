package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.com/carymatt/carved-nfs/pkg/fio"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"k8s.io/klog"
)

const (
	InitState     = 1
	RunningState  = 2
	FinishedState = 3
)

var (
	labels = []string{"job", "group"}

	readKb = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "readkb",
			Help: "Cumulative KB read",
		},
		labels,
	)
	readBW = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "readbw",
			Help: "Read bandwidth",
		},
		labels,
	)
	readIOPS = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "readiops",
			Help: "Read IOPS",
		},
		labels,
	)
	readSLat = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "readslat",
			Help: "Average read submission latency",
		},
		labels,
	)
	readCLat = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "readclat",
			Help: "Average read completion latency",
		},
		labels,
	)
	readTLat = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "readtlat",
			Help: "Average read total latency",
		},
		labels,
	)
	readSLatMin = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "readslat_min",
			Help: "Minimum read submission latency",
		},
		labels,
	)
	readCLatMin = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "readclat_min",
			Help: "Minimum read completion latency",
		},
		labels,
	)
	readTLatMin = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "readtlat_min",
			Help: "Minimum read total latency",
		},
		labels,
	)

	writeKb = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "writekb",
			Help: "Cumulative KB written",
		},
		labels,
	)
	writeBW = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "writebw",
			Help: "Write bandwidth",
		},
		labels,
	)
	writeIOPS = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "writeiops",
			Help: "Write IOPS",
		},
		labels,
	)
	writeSLat = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "writeslat",
			Help: "Average write submission latency",
		},
		labels,
	)
	writeCLat = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "writeclat",
			Help: "Average write completion latency",
		},
		labels,
	)
	writeTLat = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "writetlat",
			Help: "Average write total latency",
		},
		labels,
	)
	writeSLatMin = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "writeslat_min",
			Help: "Minimum write submission latency",
		},
		labels,
	)
	writeCLatMin = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "writeclat_min",
			Help: "Minimum write completion latency",
		},
		labels,
	)
	writeTLatMin = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "writetlat_min",
			Help: "Minimum write total latency",
		},
		labels,
	)

	runState = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "runstate",
			Help: "Run state",
		})

	seenLabels = make(map[JobLabel]struct{})
)

type JobLabel struct {
	job   string
	group string
}

func init() {
	prometheus.MustRegister(readKb)
	prometheus.MustRegister(readBW)
	prometheus.MustRegister(readIOPS)
	prometheus.MustRegister(readSLat)
	prometheus.MustRegister(readCLat)
	prometheus.MustRegister(readTLat)
	prometheus.MustRegister(readSLatMin)
	prometheus.MustRegister(readCLatMin)
	prometheus.MustRegister(readTLatMin)
	prometheus.MustRegister(writeKb)
	prometheus.MustRegister(writeBW)
	prometheus.MustRegister(writeIOPS)
	prometheus.MustRegister(writeSLat)
	prometheus.MustRegister(writeCLat)
	prometheus.MustRegister(writeTLat)
	prometheus.MustRegister(writeSLatMin)
	prometheus.MustRegister(writeCLatMin)
	prometheus.MustRegister(writeTLatMin)
	prometheus.MustRegister(runState)
}

func watch(d *fio.Datum) {
	labels := prometheus.Labels{"job": d.Job, "group": d.Group}
	seenLabels[JobLabel{job: d.Job, group: d.Group}] = struct{}{}
	readKb.With(labels).Set(d.ReadKb)
	readBW.With(labels).Set(d.ReadBandwidth)
	readIOPS.With(labels).Set(d.ReadIOPS)
	readSLat.With(labels).Set(d.ReadSLat)
	readCLat.With(labels).Set(d.ReadCLat)
	readTLat.With(labels).Set(d.ReadTLat)
	readSLatMin.With(labels).Set(d.ReadSLatMin)
	readCLatMin.With(labels).Set(d.ReadCLatMin)
	readTLatMin.With(labels).Set(d.ReadTLatMin)
	writeKb.With(labels).Set(d.WriteKb)
	writeBW.With(labels).Set(d.WriteBandwidth)
	writeIOPS.With(labels).Set(d.WriteIOPS)
	writeSLat.With(labels).Set(d.WriteSLat)
	writeCLat.With(labels).Set(d.WriteCLat)
	writeTLat.With(labels).Set(d.WriteTLat)
	writeSLatMin.With(labels).Set(d.WriteSLatMin)
	writeCLatMin.With(labels).Set(d.WriteCLatMin)
	writeTLatMin.With(labels).Set(d.WriteTLatMin)
}

func zeroStats() {
	for l := range seenLabels {
		labels := prometheus.Labels{"job": l.job, "group": l.group}
		readKb.With(labels).Set(0)
		readBW.With(labels).Set(0)
		readIOPS.With(labels).Set(0)
		readSLat.With(labels).Set(0)
		readCLat.With(labels).Set(0)
		readTLat.With(labels).Set(0)
		writeKb.With(labels).Set(0)
		writeBW.With(labels).Set(0)
		writeIOPS.With(labels).Set(0)
		writeSLat.With(labels).Set(0)
		writeCLat.With(labels).Set(0)
		writeTLat.With(labels).Set(0)
	}
}

func getStatefulSetOrdinal() (int, error) {
	hostname := os.Getenv("HOSTNAME")
	i := strings.LastIndex(hostname, "-")
	if i < 0 {
		return -1, fmt.Errorf("No - in hostname: `%s'", hostname)
	}
	ordinal, err := strconv.Atoi(hostname[i+1:])
	if err != nil {
		return -1, fmt.Errorf("No ordinal in hostname `%s': %w", hostname, err)
	}
	return ordinal, nil
}

func readPodStart(podOrdinal int) (time.Time, error) {
	startfile := filepath.Join(fmt.Sprintf("/data/start-%d", podOrdinal))
	if _, err := os.Stat(startfile); os.IsNotExist(err) {
		return time.Time{}, err
	}
	timestr, err := ioutil.ReadFile(startfile)
	if err != nil {
		return time.Time{}, err
	}
	secs, err := strconv.ParseInt(strings.TrimSpace(string(timestr)), 10, 64)
	if err != nil {
		return time.Time{}, err
	}
	return time.Unix(secs, 0), nil
}

func writePodStart(podOrdinal int) error {
	startfile := filepath.Join(fmt.Sprintf("/data/start-%d", podOrdinal))
	if _, err := os.Stat(startfile); err == nil {
		klog.Warningf("Startfile %s already exists", startfile)
	}
	tmp, err := ioutil.TempFile("/data", fmt.Sprintf("tmp-%d-", podOrdinal))
	tmpname := tmp.Name()
	if err != nil {
		return err
	}
	tmp.WriteString(fmt.Sprintf("%v\n", time.Now().Unix()))
	tmp.Close()
	if err := syscall.Rename(tmpname, startfile); err != nil {
		return err
	}
	return nil
}

func main() {
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		klog.Fatal(http.ListenAndServe(":8080", nil))
	}()

	runState.Set(InitState)

	ordinal, err := getStatefulSetOrdinal()
	if err != nil {
		klog.Fatalf("Can't get pod ordinal: %v", err)
	}

	runner, err := fio.NewFioRunner(ordinal, "hammer-data.fio")
	if err != nil {
		klog.Fatalf("Can't create runner: %v", err)
	}
	runner.AddWatcher(watch)

	if ordinal > 0 {
		previous, err := readPodStart(ordinal - 1)
		for os.IsNotExist(err) {
			time.Sleep(15 * time.Second)
			previous, err = readPodStart(ordinal - 1)
		}
		if err != nil {
			klog.Fatalf("Can't read start file of previous pod (%d): %v", ordinal-1, err)
		}
		wait := previous.Add(2 * time.Minute).Sub(time.Now())
		klog.Infof("Waiting %v to start", wait)
		time.Sleep(wait)
	} else {
		// We're pod 0; remove /data/start-* so everyone can write their startfiles.
		datadir, err := os.Open("/data")
		if err != nil {
			klog.Fatalf("Can't open /data to clean up startfiles: %v", err)
		}
		names, err := datadir.Readdirnames(0)
		if err != nil {
			klog.Fatalf("Can't read /data to clean up startfiles: %v", err)
		}
		for _, name := range names {
			if strings.HasPrefix(name, "start-") {
				os.Remove(filepath.Join("/data/", name))
			}
		}
	}
	err = writePodStart(ordinal)
	if err != nil {
		klog.Fatalf("Can't write starttime: %v", err)
	}

	done := make(chan struct{})
	runner.AddFinisher(done)

	_, err = runner.Start(context.Background())
	if err != nil {
		klog.Fatalf("Can't start runner: %v", err)
	}
	runState.Set(RunningState)

	<-done
	zeroStats()
	runState.Set(FinishedState)

	waitForever := make(chan struct{})
	<-waitForever
}
