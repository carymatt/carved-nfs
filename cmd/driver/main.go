package main

import (
	"flag"
	"os"

	"gitlab.com/carymatt/carved-nfs/pkg/driver"
	"gitlab.com/carymatt/carved-nfs/pkg/server"

	"k8s.io/klog"
)

var (
	endpoint = flag.String("endpoint", "unix:/tmp/csi.sock", "CSI endpoint")
)

func init() {
	klog.InitFlags(flag.CommandLine)
	flag.Set("lottostderr", "true")
}

func main() {
	flag.Parse()

	identityServer, err := driver.NewIdentityServer()
	if err != nil {
		klog.Fatalf("Failed to create identity server: %w", err)
	}
	controllerServer, err := driver.NewControllerServer()
	if err != nil {
		klog.Fatalf("Failed to create controller server: %w", err)
	}
	nodeServer, err := driver.NewNodeServer()
	if err != nil {
		klog.Fatalf("Failed to create node server: %w", err)
	}

	s := server.NewNonBlockingGRPCServer()
	s.Start(*endpoint, identityServer, controllerServer, nodeServer)
	s.Wait()

	os.Exit(0)
}
